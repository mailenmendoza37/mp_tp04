'''Utilizando diccionarios diseñar un programa modular que permita gestionar los productos de un comercio,
las funcionalidades solicitadas son:
a. Registrar productos: para cada uno se debe solicitar, código, descripción, precio y stock. Agregar las
siguientes validaciones:
i. El código no se puede repetir
ii. Todos los valores son obligatorios
iii. El precio y el stock no pueden ser negativos
b. Mostrar el listado de productos
c. Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]
d. Diseñar un proceso que le sume X al stock de todos los productos cuyo valor actual de stock sea menor
al valor Y.
e. Eliminar todos los productos cuyo stock sea igual a cero.
f. Salir'''
import os
def menuP():
    print('*** Menu Principal *** \n a) Registrar Producto \n b) Lista de Productos \n c) Mostrar producto por Stock \n d) Suma de Stock \n e)  Eliminar todos producto cuyo stock es cero ')
    eleccion = input('Ingrese una opcion: ')
    return eleccion



def registrarP():
    codigo=1
    productos={}
    
    print(' ***Ingrese Datos del Producto *** ')
    while( codigo!=0):
        nombreP = input('Nombre del Producto: ')
        codigo = int(input('Ingrese Codigo del Producto (cero para finalizar): '))
        if codigo != 0: 
            
            if codigo not in productos: 
                precio= int(input("Ingrese precio del Producto: ") )
                while precio<0:
                    print("El precio debe ser un valor positivo")  
                    precio= int(input("Ingrese precio del Producto: ") )
                stock=int(input("Ingrese Stock: ") )
                while stock<0:
                    print("El STOCK debe ser un valor positivo")  
                    precio= int(input("Ingrese Stock: ") )
                productos[codigo] = {nombreP,precio, stock} 
                print(' Producto Agregado Correctamente')
            else:
                print('***El codigo ya existe***')
        elif nombreP=='' and codigo=='':
            print("DEBE INGRESAR UN VALOR PARA CONTINUAR")
    return productos


def mostrarRango(productos):
    os.system('cls')
    cont=0
    cont2=len(productos)
    A=int(input('Minimo valor del intervalo: '))
    B=int(input('Maximo valor del intervalo: '))
    while B<A:
        print('El valor Minimo debe ser menor al valor Maximo')
        A=int(input('Minimo valor del intervalo: '))
        B=int(input('Maximo valor del intervalo: '))
    print('')
    print(f'Lista de productos dentro del rango de Stock {A} hasta {B}: ')
    for codigo in productos:
        if productos[codigo][2]>= A and productos[codigo][2]<=B:
            print(codigo,productos[codigo])
        else:
            cont+=1
    if cont==cont2:
        print('No se encontro ningun Stock')

def suma(productos):
    os.system('cls')
    X=int(input('Ingrese el valor de X: '))
    Y=int(input('Ingrese el valor de Y: '))
    cont=0
    cont2=len(productos)
    print(f'Productos a los cuales se le a sumado {X} :')
    for codigo in productos:
        if productos[codigo][2]<Y:
            productos[codigo][2]+=X
            print(codigo,productos[codigo])
        else:
            cont+=1
    if cont==cont2:
        print('No se encontro ningun Stock')

def verificar(productos):
    pos=-1
    for codigo,valor in productos.items():
        if valor[2] == 0:
            pos= codigo
            break
    return pos

def eliminar(productos):
    os.system 
    pos=1
    while pos!=-1:
        pos=verificar(productos)
        if pos==-1:
            registrarP(productos)
        else:
            del productos[pos] 
    return productos


op=''

while (op != 'f'):
    os.system('cls')
    op = menuP()
    if op == 'a':
      productos = registrarP()
    elif op== 'b':
       print(f'*****Productos Ingresados**** \n{productos}')
    elif op == 'c':
        mostrarRango(productos)
    elif op == 'd':
       suma(productos)
    elif op == 'e':
      eliminar(productos)
    elif op == 'f':
       print("Fin del Programa")  
    else:
        print("   ERROR....OPCION NO VALIDA ") 
    input ('Pulse ENTER para regresar al menu...')
